public class RefactorRepresentationOfNumber {
    public static void main(String[] args) {
        FirstWay(5);
        FirstWay(-5);
        SecondWay(2.5);
        SecondWay(-2.5);
    }

    private static void FirstWay(int i) {
        String intBits=Integer.toBinaryString(i);
        System.out.println("Разряды числа "+i +intBits);
    }
    private static void SecondWay(double v) {
        String doubleBits=Long.toBinaryString((long) v);
        System.out.println("Разряды числа '-2.5' "+doubleBits);
    }
}
